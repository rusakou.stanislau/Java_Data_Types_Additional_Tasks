import java.util.Scanner;

public class Main {
    private static Scanner scanner;

    public static void main(String[] arg) {

        scanner = new Scanner(System.in);

        System.out.println("\n Please Enter the 1st Digit: ");
        double digit1stToProvideBasicMath = scanner.nextDouble();
        System.out.println("\n Please Enter the 2nd Digit: ");
        double digit2ndToProvideBasicMath = scanner.nextDouble();

        provideBasicMathOperationsFor2Digits(digit1stToProvideBasicMath, digit2ndToProvideBasicMath);

        System.out.println("\n Please Enter the 3nd Digit to Calculate the Value of the Formula: ");
        double digit3rdToCalculateFormulaValue = scanner.nextDouble();

        findValueOf1stMathFunction(digit1stToProvideBasicMath, digit2ndToProvideBasicMath, digit3rdToCalculateFormulaValue);

        System.out.println("\n Please Enter the Length of a Triangle's Leg A: ");
        double legA = scanner.nextDouble();
        System.out.println("\n Please Enter the Length of a Triangle's Leg B: ");
        double legB = scanner.nextDouble();

        calculatePerimeterAndAreaOfRightTriangle(legA, legB);

        System.out.println("\n Please Enter the Digit to Find the Product of the Digits of a Given Number: ");
        int givenNumberToFindProductOfDigits = (int) scanner.nextDouble();
        findProductOfDigitsOfGiven4DigitNumber(givenNumberToFindProductOfDigits);
    }

    public static void provideBasicMathOperationsFor2Digits(double x, double y) {
        double sum = x + y;
        System.out.println("sum="+sum);

        double difference = x - y;
        System.out.println("difference="+difference);

        double product = x * y;
        System.out.println("product="+product);

        double quotient = x / y;
        System.out.println("quotient="+quotient);
    }

    public static void findValueOf1stMathFunction(double a, double b, double c) {
        double result = ((a - 3) * b / 2) + c;
        System.out.println("result="+result);
    }

    public static void calculatePerimeterAndAreaOfRightTriangle(double a, double b) {
        double c = Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2));

        double perimeter = a + b + c;
        System.out.println("perimeter="+perimeter);

        double area = 0.5 * a * b;
        System.out.println("area="+area);
    }

    private static void findProductOfDigitsOfGiven4DigitNumber(int number) {
        int result = 1;

        while (number > 0) {
            result *= number % 10;
            number /= 10;
        }

        System.out.println("product="+result);
    }
}
